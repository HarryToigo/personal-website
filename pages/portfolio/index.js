import Link from 'next/link';
import styles from '../../styles/portfolio.module.css';

export default function Portfolio() {
  return (
    <article className={styles.portfolioScreen}>
      <div className={styles.heading}>
        <h1 className={styles.title}>Design|Dev Portfolio</h1>
        <h2 className={styles.description}>
          A selection of some of the development and design work I have done.
        </h2>
      </div>

      <div className={styles.worksList}>
        <Link href="/portfolio/substrate">
          <a className={styles.workCard}>
            <h2>Substrate.JS</h2>
            <p>
              A port of Jared S Tarbell&rsquo;s <em>Substrate</em> to the modern
              web.
            </p>
          </a>
        </Link>

        <Link href="/portfolio/loremipsum">
          <a className={styles.workCard}>
            <h2>Lorem Ipsum</h2>
            <p>
              Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
              eiusmod tempor incididunt ut labore et dolore magna aliqua.
            </p>
          </a>
        </Link>

        <Link href="/portfolio/loremipsum">
          <a className={styles.workCard}>
            <h2>Ut enim ad minim</h2>
            <p>
              Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris
              nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in
              reprehenderit in voluptate velit esse cillum dolore eu fugiat
              nulla pariatur.
            </p>
          </a>
        </Link>

        <Link href="/portfolio/loremipsum">
          <a className={styles.workCard}>
            <h2>Malesuada fames</h2>
            <p>
              Malesuada fames ac turpis egestas maecenas. Odio pellentesque diam
              volutpat commodo sed egestas egestas fringilla phasellus. Dapibus
              ultrices in iaculis nunc sed.
            </p>
          </a>
        </Link>

        <Link href="/portfolio/loremipsum">
          <a className={styles.workCard}>
            <h2>Convallis tellus</h2>
            <p>
              Convallis tellus id interdum velit laoreet id. Laoreet sit amet
              cursus sit amet.
            </p>
          </a>
        </Link>
      </div>
    </article>
  );
}
