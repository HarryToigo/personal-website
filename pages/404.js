import Link from 'next/link';
import styles from '../styles/custom404.module.css';

export default function Custom404() {
  return (
    <article className={styles.custom404Screen}>
      <div className={styles.errorCodeAndMsg}>
        <h1 className={styles.errorCode}>404</h1>
        <h2 className={styles.errorMsg}>That page could not be found.</h2>
      </div>
      <div className={styles.explanation}>
        <p>
          I&rsquo;m sorry, but I seem to have misplaced that. You may send me an
          email describing the problem, if you&rsquo;d like.
        </p>
        <p>
          Or you may just go back to{' '}
          <Link href="/">
            <a className={styles.link}>the home page</a>
          </Link>
          .
        </p>
      </div>
    </article>
  );
}
