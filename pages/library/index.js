import Link from 'next/link';
import styles from '../../styles/library.module.css';

export default function Library() {
  return (
    <article className={styles.libraryScreen}>
      <div className={styles.heading}>
        <h1 className={styles.title}>The Library</h1>
        <h2 className={styles.description}>
          Books, articles and papers that have been influential to me or that I
          think are worth reading.
        </h2>
      </div>

      <div className={styles.bookList}>
        <Link href="/library/GEB">
          <a className={styles.bookCard}>
            <h2>Godel, Escher, Bach</h2>
            <p>This is perhaps my favorite book of all-time.</p>
          </a>
        </Link>

        <Link href="/library/semper">
          <a className={styles.bookCard}>
            <h2>Semper eget duis!</h2>
            <p>
              Duis tristique sollicitudin nibh sit amet commodo nulla facilisi
              nullam vehicula.
            </p>
          </a>
        </Link>
        <Link href="/library/placerat">
          <a className={styles.bookCard}>
            <h2>Placerat duis ultricies!</h2>
            <p>
              Enim, sed faucibus turpis in eu mi bibendum neque egestas congue.
            </p>
          </a>
        </Link>
        <Link href="/library/malesuada">
          <a className={styles.bookCard}>
            <h2>Malesuada pellentesque elit</h2>
            <p>
              Augue ut lectus arcu, bibendum at varius vel, pharetra vel turpis.
            </p>
          </a>
        </Link>
        <Link href="/library/atquis">
          <a className={styles.bookCard}>
            <h2>At quis risus</h2>
            <p>
              Pretium viverra suspendisse potenti nullam ac tortor vitae purus
              faucibus ornare!
            </p>
          </a>
        </Link>
        <Link href="/library/mollis">
          <a className={styles.bookCard}>
            <h2>Mollis nunc sed!</h2>
            <p>
              Fames ac turpis egestas integer eget aliquet nibh praesent
              tristique magna.
            </p>
          </a>
        </Link>
        <Link href="/library/sitamet">
          <a className={styles.bookCard}>
            <h2>Sit amet facilisis</h2>
            <p>
              Pharetra vel turpis nunc eget lorem dolor, sed viverra ipsum nunc.
            </p>
          </a>
        </Link>
      </div>
    </article>
  );
}
