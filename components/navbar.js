import Link from 'next/link';
import Monogram from '../components/monogram';
import styles from '../styles/navbar.module.css';

export default function Navbar() {
  return (
    <header className={styles.header}>
      <nav className={styles.navbar}>
        <Link className={styles.homelink} href="/">
          <a>
            <Monogram className={styles.monogram} />
          </a>
        </Link>
        <div className={styles.otherlinks}>
          <Link href="/portfolio">
            <a className={styles.otherlink}>Portfolio</a>
          </Link>
          <Link href="/garden">
            <a className={styles.otherlink}>Digital Garden</a>
          </Link>
          <Link href="/blog">
            <a className={styles.otherlink}>Blog</a>
          </Link>
          <Link href="/library">
            <a className={styles.otherlink}>Library</a>
          </Link>
          <Link href="/about">
            <a className={styles.otherlink}>About</a>
          </Link>
        </div>
      </nav>
    </header>
  );
}
