import Head from 'next/head';

export default function SiteMeta() {
  return (
    <Head>
      <title>Harry H. Toigo</title>
      <meta
        name="description"
        content="The personal website of Harry H. Toigo II."
      />
      <link rel="icon" href="/favicon-48x48.png" />

      {/*
       *  <link rel="icon" type="image/png" href="/favicon-32x32.png" />
       */}
    </Head>
  );
}
